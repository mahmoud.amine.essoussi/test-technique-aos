


## Pre-requis
Pour faire le build et démarrer l'application, il faut:
* Installer Node.js
* Installer MongoDB

## Installation

* cloner le projet ('https://gitlab.com/mahmoud.amine.essoussi/test-technique-aos')
* installer les dépendences:
```
cd <project_name>
npm install
```

## setup de l'environnement

* Créer un fichier .env sous le root du projet 

```
PORT=3001
host_mongo=127.0.0.1
port_mongo=27017
auth_encryption_salt=secret
```


## Démarrer l'application 



```
 npm run start:dev
```

Ouvrir le navigateur et aller sur l'adresse: http://localhost:3001/graphql 

Vous aller trouver le client playground graphQL là où on va faire les opérations suivantes: 

## Scénarios

Créer un user 

```
 mutation {
  addUser(
    input: {
      email: "exemple@exemple.com"
      name: "exemple exemple"
      password:"exemple"
    }
  ) {
    name
    email
  }
}
```

Login

```
mutation {
  login(      email: "exemple@exemple.com"
      password:"exemple")
}
```

Créer une tâche

```
mutation {
  addTache(
    input: {
			title:"tache 1",
      description:"description",
      body:"body"
    }
  ) {
    title
    description
    status
  }
}
```

Afficher les tâches

```
query {
taches
{title}
}
```

Afficher une tâche par Id

```
query {
tache(
  	id:"60aef348d3df3b22840d367d"
  ) {
    title
    description
    user {email}
  }

}
```

Modifier une tâche par Id 

```
mutation {
updateTache(
  	id:"60aef7b45c539d1ef818cef2",
    input: {
      title:"tache 2",
      description:"description",
      body:"body",
    }
  ) {
    title
    description
    status
    user {email}
  }
}
```

Modifier le statut d'une tâche par Id

```
mutation {
  updateStatusTache(
		id:"60aef7b45c539d1ef818cef2",
    status:"non complétée"
  ) {
    title
    description
    status
  }
}
```

Supprimer une tâche par Id

```
mutation {
deleteTache(
  	id:"60aeda0fcd7ddf12581c4b0f"
){title}

}
```

Ajouter un commentaire sur une tâche (Id)

```
mutation {
  addComment (
    tacheId:"60aeda0fcd7ddf12581c4b0f"
    commentDescription:"this is a hard task"
  ) {comments{comment user{email}}
    }
}
```







