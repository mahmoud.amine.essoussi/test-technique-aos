import { VerifyAuthorization } from '../decorators/auth.decorator';
import { Context } from '../models/context';
import * as jwt from 'jsonwebtoken';
const Comments = require('../models/comments');
const Taches = require('../models/taches');
const Users = require('../models/users');

export class CommentsController {
  @VerifyAuthorization
  async addComment(inputObject: any, _ctx: Context) {
    const userMongoId = await Users.findOne({ email: _ctx.email }).then((userObject: any) => {
      return userObject._id;
    });

    return  Comments.create({ comment: inputObject.commentDescription, user: userMongoId })
      .then((commentInfo: any) => {

        return Taches.findOneAndUpdate(
          {
            _id: inputObject.tacheId
          },
          { $push: { comments: commentInfo._id } },
          { new: true }
        ).populate({
          path: 'comments',
          model: 'Comment',
          populate: {
            path: 'user',
            model: 'User',
          },
        });
      })
      .catch((err: any) => {
        throw err;
      });
  }

}
