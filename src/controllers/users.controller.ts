import { Context } from '../models/context';
import bcrypt from 'bcryptjs';
import { error } from 'console';

const Users = require('../models/users');

export class UsersController {
  async  registerUser(inputObject: any, ctx: Context) {
    const user = await Users.findOne({ email: inputObject.input.email }).then((userObject: any) => {
      return userObject;
    });

      if (!user){
        const hashedPassword = await bcrypt.hash(inputObject.input.password, 12) ;
        inputObject.input.password = hashedPassword ;
          return Users.create(inputObject.input).then((userInfo: any) => {
            return userInfo;
        });
      } 
      else {
        throw new Error('email deja utilisé')
      }



  }


  updateUser(inputObject: any, ctx: Context) {
    return Users.findOneAndUpdate({ _id: inputObject.id }, inputObject.input, { new: true }).then(
      (userInfo: any) => {
        return userInfo;
      }
    );
  }
}
