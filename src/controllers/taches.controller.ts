import { Context } from '../models/context';
import { VerifyAuthorization } from '../decorators/auth.decorator';

const Taches = require('../models/taches');
const Users = require('../models/users');

export class TachesController {
  @VerifyAuthorization
  getTache(args: any, ctx: Context) {
    return Taches.findOne({ _id: args['id'] })
      .populate('user')
      .then((tache: any) => {
        return tache;
      });
  }

  @VerifyAuthorization
  getTaches(args: any, ctx: any) {
    return Taches.find()
      .populate('user')
      .then((taches: any) => {
        console.log(taches)
        return taches;
      });
  }



  @VerifyAuthorization
  async updateTache(inputObject: any, ctx: any) {
        return Taches.findOneAndUpdate({ _id: inputObject.id }, inputObject.input, { new: true }).populate('user').then(
          (tacheInfo: any) => {
            return tacheInfo;
          }
        );
  }

  @VerifyAuthorization
  async deleteTache(inputObject: any, ctx: any) {

    const user = await Users.findOne({ email: ctx.email }).then((userObject: any) => {
      return userObject;
    });

    const tache = await Taches.findOne({  _id: inputObject.id }).populate('user');
    if ( !tache ){
      throw new Error('No tache found ')
    } else {
      if (JSON.stringify(tache.user) === JSON.stringify(user) ) {
        return Taches.findOneAndDelete({ _id: inputObject.id }).then((tacheInfo: any) => {
          return tacheInfo;
        });
      } else {
        throw new Error('ce user na pas le droit de supprimer cette tache')
      }
    }

  }

  @VerifyAuthorization
  async updateStatusTache(inputObject: any, ctx: any) {
      
    const user = await Users.findOne({ email: ctx.email }).then((userObject: any) => {
      return userObject;
    });

    const tache = await Taches.findOne({  _id: inputObject.id }).populate('user');
    if ( !tache ){
      throw new Error('aucune tache trouvee ')
    } else {
      
      if (JSON.stringify(tache.user) === JSON.stringify(user) ) {
        console.log("heyy")
        return Taches.findOneAndUpdate({_id:inputObject.id},{ status: inputObject.status },{runValidators: true}).then((tacheInfo: any) => {
          return tacheInfo;
        });
      } else {
        throw new Error('ce user na pas le droit de changer le status de cette tache')
      }
    }

  }


  @VerifyAuthorization
  async  addTache(inputObject: any, ctx: any) {

    const userMongoId = await Users.findOne({ email: ctx.email }).then((userObject: any) => {
      return userObject._id;
    });
    
    inputObject.input.user = userMongoId;

    return Taches.create(inputObject.input).then((tacheInfo: any) => {
      return tacheInfo;
    });
  }
}
