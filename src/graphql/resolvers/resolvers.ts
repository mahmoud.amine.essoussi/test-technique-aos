import { GraphQLResolveInfo } from 'graphql';
import { Context } from '../../models/context';
import { IResolvers } from 'graphql-tools';
import * as jwt from 'jsonwebtoken';
import { CommentsController } from '../../controllers/comments.controller';
import { AppConstants } from '../../constants/app.constants';
import { UsersController } from '../../controllers/users.controller';
import { TachesController } from '../../controllers/taches.controller';
import bcrypt from 'bcryptjs';


const usersController = new UsersController(); 
const tachesController = new TachesController(); 
const commentsController = new CommentsController();

const Users = require('../../models/users');

const resolvers: IResolvers = {
  Query: {
    taches: (_: void, args: any, ctx: Context, _info: GraphQLResolveInfo) => {
      return tachesController.getTaches(args, ctx);
    },
    tache: (_: void, args: any, ctx: Context, _info: GraphQLResolveInfo) => {
      return tachesController.getTache(args, ctx);
    },
  },

  Mutation: {
    async login(_, { email, password }, ctx: Context) {
      const user = await Users.findOne({ email })
      if (!user) {
        throw new Error('No user found ')
      }
      const isValid = await bcrypt.compare(password, user.password)
      if (!isValid) {
        throw new Error('Incorrect password ')
      }
      return jwt.sign({ data: email }, <string>process.env.auth_encryption_salt);
    },

    register: (_, inputObject, ctx: Context) => {
      return usersController.registerUser(inputObject, ctx);
    },
    updateUser: (_, inputObject, ctx: Context) => {
      return usersController.updateUser(inputObject, ctx);
    },
    addTache: (_, inputObject, ctx: Context) => {
      return tachesController.addTache(inputObject, ctx);
    },
    updateTache: (_, inputObject, ctx: Context) => {
      return tachesController.updateTache(inputObject, ctx);
    },
    updateStatusTache: (_, inputObject, ctx: Context) => {
      return tachesController.updateStatusTache(inputObject, ctx);
    },
    deleteTache: (_, inputObject, ctx: Context) => {
      return tachesController.deleteTache(inputObject, ctx);
    },
    addComment: (_, inputObject, ctx: Context) => {
      return commentsController.addComment(inputObject, ctx);
    },
  },
};

export default resolvers;
