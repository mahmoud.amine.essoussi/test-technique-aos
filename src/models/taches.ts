import mongoose = require('mongoose');
require('./comments');
require('./users');

const TacheSchema = new mongoose.Schema(
  {
    title: { type: String, required: true },
    description: String,
    body: { type: String, required: true },
    status: { 
       type: String,
      enum: ['complétée', 'non complétée'],
      default : 'non complétée'
     },
    comments: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Comment',
      },
    ],
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
      required: true
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model('Tache', TacheSchema);
